/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_object.h>
#include "test_plugin.h"
#include "multisettings.h"

static const char* odl_defs = "test_profile.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;


static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_plugin_trigger_expr_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);
    multisettings_plugin_init(&dm, &parser);
    handle_events();

    return 0;

}

int test_plugin_trigger_expr_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    _multisettings_main(1, NULL, NULL);
    return 0;
}

void test_plugin_trigger_expr(UNUSED void** state) {
    amxd_object_t* trigger = amxd_dm_findf(&dm, "MultiSettings.Profile.1.Trigger.1");
    amxc_string_t expr;

    assert_non_null(trigger);
    amxc_string_init(&expr, 0);
    assert_int_equal(build_expression_from_trigger(trigger, &expr), 0);
    assert_string_equal("notification in 'dm:object-changed' && parameters.State.to == 'Running'", amxc_string_get(&expr, 0));
    amxc_string_clean(&expr);
}

void test_plugin_trigger_not_expr(UNUSED void** state) {
    amxd_object_t* trigger = amxd_dm_findf(&dm, "MultiSettings.Profile.1.Trigger.1");
    amxc_string_t expr;

    assert_non_null(trigger);
    amxc_string_init(&expr, 0);
    assert_int_equal(build_not_expression_from_trigger(trigger, &expr), 0);
    assert_string_equal("notification in 'dm:object-changed' && parameters.State.from == 'Running'", amxc_string_get(&expr, 0));
    amxc_string_clean(&expr);
}

void test_plugin_trigger_get_expr(UNUSED void** state) {
    amxd_object_t* trigger = amxd_dm_findf(&dm, "MultiSettings.Profile.1.Trigger.1");
    amxc_string_t expr;

    assert_non_null(trigger);
    amxc_string_init(&expr, 0);
    assert_int_equal(build_expression_get_from_trigger(trigger, &expr), 0);
    assert_string_equal("0.[State == 'Running']", amxc_string_get(&expr, 0));
    amxc_string_clean(&expr);
}

void test_plugin_trigger_matches_expr(UNUSED void** state) {
    amxd_object_t* trigger = amxd_dm_findf(&dm, "MultiSettings.Profile.1.Trigger.2");
    amxc_string_t expr;

    assert_non_null(trigger);
    amxc_string_init(&expr, 0);
    assert_int_equal(build_expression_from_trigger(trigger, &expr), 0);
    assert_string_equal("notification in 'dm:object-changed' && parameters.State.to matches '^.{2}nn.{3}$'", amxc_string_get(&expr, 0));
    amxc_string_clean(&expr);
}

void test_plugin_trigger_matches_not_expr(UNUSED void** state) {
    amxd_object_t* trigger = amxd_dm_findf(&dm, "MultiSettings.Profile.1.Trigger.2");
    amxc_string_t expr;

    assert_non_null(trigger);
    amxc_string_init(&expr, 0);
    assert_int_equal(build_not_expression_from_trigger(trigger, &expr), 0);
    assert_string_equal("notification in 'dm:object-changed' && parameters.State.from matches '^.{2}nn.{3}$'", amxc_string_get(&expr, 0));
    amxc_string_clean(&expr);
}