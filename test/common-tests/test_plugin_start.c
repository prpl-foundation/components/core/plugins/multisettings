/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#define _GNU_SOURCE
#include <stdlib.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_object.h>
#include "test_plugin.h"
#include "multisettings.h"

static const char* odl_defs = "test.odl";

static amxd_dm_t dm;
static amxo_parser_t parser;


static void handle_events(void) {
    printf("Handling events ");
    while(amxp_signal_read() == 0) {
        printf(".");
    }
    printf("\n");
}

int test_plugin_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    handle_events();

    return 0;

}

int test_plugin_teardown(UNUSED void** state) {
    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);
    return 0;
}

void test_plugin_start(UNUSED void** state) {
    assert_int_equal(_multisettings_main(0, &dm, &parser), 0);

    assert_non_null(multisettings_plugin_get_parser());
    assert_ptr_equal(multisettings_plugin_get_parser(), &parser);

    assert_non_null(multisettings_plugin_get_config());
    assert_ptr_equal(multisettings_plugin_get_config(), &parser.config);

}

void test_plugin_start_with_profile_set(UNUSED void** state) {
    amxd_object_t* ms = amxd_dm_findf(&dm, "MultiSettings");
    amxd_dm_t profile_dm;

    assert_non_null(ms);

    amxd_dm_init(&profile_dm);

    assert_int_equal(amxd_object_set_value(cstring_t, ms, "CurrentProfile", "test_profile"), amxd_status_ok);
    assert_int_equal(amxd_object_set_value(cstring_t, ms, "DecisionMadeBy", "System"), amxd_status_ok);
    assert_int_equal(_multisettings_main(0, &dm, &parser), 0);
    start_multisettings();

    amxd_object_t* root_obj = amxd_dm_get_root(&profile_dm);
    amxc_var_t* setting = amxo_parser_get_config(&parser, "selected_profile_file");
    assert_non_null(setting);
    assert_int_equal(amxo_parser_parse_file(&parser, amxc_var_constcast(cstring_t, setting), root_obj), 0);
    assert_string_equal(amxc_var_constcast(cstring_t, amxo_parser_get_config(&parser, "profile")), "test_profile");

    amxd_dm_clean(&profile_dm);
}

void test_plugin_write_enable_true(UNUSED void** state) {
    amxc_var_t enable;
    amxc_var_t parameter;
    amxc_var_t values;

    amxc_var_init(&parameter);
    amxc_var_init(&enable);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "to", "true");
    amxc_var_add_key(cstring_t, &values, "from", "false");
    amxc_var_set_type(&enable, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &enable, "Enable", amxc_var_constcast(amxc_htable_t, &values));
    amxc_var_set_type(&parameter, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &parameter, "parameter", amxc_var_constcast(amxc_htable_t, &enable));
    _writeEnable(NULL, &parameter, NULL);
    amxc_var_clean(&values);
    amxc_var_clean(&enable);
    amxc_var_clean(&parameter);
}

void test_plugin_write_enable_false(UNUSED void** state) {
    amxc_var_t enable;
    amxc_var_t parameter;
    amxc_var_t values;

    amxc_var_init(&parameter);
    amxc_var_init(&enable);
    amxc_var_init(&values);
    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "from", "true");
    amxc_var_add_key(cstring_t, &values, "to", "false");
    amxc_var_set_type(&enable, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &enable, "Enable", amxc_var_constcast(amxc_htable_t, &values));
    amxc_var_set_type(&parameter, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(amxc_htable_t, &parameter, "parameter", amxc_var_constcast(amxc_htable_t, &enable));
    _writeEnable(NULL, &parameter, NULL);
    amxc_var_clean(&values);
    amxc_var_clean(&enable);
    amxc_var_clean(&parameter);
}


void test_plugin_stop(UNUSED void** state) {
    assert_int_equal(_multisettings_main(1, &dm, &parser), 0);
}
