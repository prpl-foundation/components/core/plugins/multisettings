/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include "multisettings.h"

int get_modules_list(amxc_var_t* list, const char* modules) {
    int ret = -1;
    amxc_string_t to_split;
    const char* reason;

    amxc_string_init(&to_split, strlen(modules));
    amxc_string_set(&to_split, modules);
    if(amxc_string_csv_to_var(&to_split, list, &reason) == AMXC_STRING_SPLIT_OK) {
        ret = 0;
    }
    amxc_string_clean(&to_split);
    return (ret);
}

int module_is_running(const char* module) {
    int ret;
    amxc_string_t pid;

    amxc_string_init(&pid, 0);
    amxc_string_setf(&pid, "/var/run/%s.pid", module);
    ret = access(amxc_string_get(&pid, 0), F_OK);
    amxc_string_clean(&pid);
    return (ret == 0);
}

//[TEMP] delete the save file for the impacted plugin so it
//dosen't interfere with the new configuration from the profile.
static void stop_module(const char* module) {
    amxc_string_t cmd;
    amxp_subproc_t* proc = NULL;

    amxc_string_init(&cmd, 0);
    amxc_string_setf(&cmd, "/etc/init.d/%s", module);
    SAH_TRACEZ_INFO(ME, "stopping module [%s]", module);
    when_failed(amxp_subproc_new(&proc), stop);

    amxp_subproc_start_wait(proc, 2000, (char*) amxc_string_get(&cmd, 0), "stop", NULL);
    amxp_subproc_delete(&proc);
stop:
    amxc_string_clean(&cmd);
}

//[TEMP] delete the save file for the impacted plugin so it
//dosen't interfere with the new configuration from the profile.
static void delete_module_save_file(const char* module) {
    amxc_string_t save_folder;
    amxp_subproc_t* proc = NULL;

    amxc_string_init(&save_folder, 0);
    amxc_string_setf(&save_folder, "/etc/config/%s/odl", module);
    SAH_TRACEZ_INFO(ME, "deleting folder /etc/config/%s/odl", module);

    when_failed(amxp_subproc_new(&proc), stop);
    amxp_subproc_start_wait(proc, 2000, (char*) "rm", "-rf", amxc_string_get(&save_folder, 0), NULL);
    amxp_subproc_delete(&proc);
stop:
    amxc_string_clean(&save_folder);
}

//[TEMP] Just call restart??
static void start_module(const char* module) {
    SAH_TRACEZ_INFO(ME, "starting module [%s]", module);
    amxc_string_t cmd;
    int ret = -1;

    amxc_string_init(&cmd, 0);
    amxc_string_setf(&cmd, "/etc/init.d/%s start", module);
    ret = system(amxc_string_get(&cmd, 0)); //[TEMP] to be removed
    if(ret == -1) {
        SAH_TRACEZ_ERROR(ME, "failed to start [%s]", module);
    }
    amxc_string_clean(&cmd);
}

//[TEMP] To be removed
static void check_pid_file_cb(amxp_timer_t* timer, void* priv) {
    char* module = (char*) priv;
    amxc_string_t pid_file;

    amxc_string_init(&pid_file, 0);
    amxc_string_setf(&pid_file, "/var/run/%s.pid", module);
    SAH_TRACEZ_INFO(ME, "check process [%s]", module);

    if(access(amxc_string_get(&pid_file, 0), F_OK) != 0) {
        SAH_TRACEZ_INFO(ME, "pid deleted clean module %s", module);
        amxp_timer_stop(timer);
        amxp_timer_delete(&timer);
        delete_module_save_file(module);
        start_module(module);
        free(module);
    }
    amxc_string_clean(&pid_file);
}

//[TEMP] To be removed
static void wait_for_process(const char* module) {
    char* proc_name = strdup(module);
    amxp_timer_t* check_pid_timer = NULL;
    amxp_timer_new(&check_pid_timer, check_pid_file_cb, (void*) proc_name);
    amxp_timer_set_interval(check_pid_timer, 500);
    amxp_timer_start(check_pid_timer, 500);
}

//[TEMP] To be removed
//Just call restart for each module!!!
static void handle_impacted_module(const char* module) {

    if(!module_is_running(module)) {
        SAH_TRACEZ_INFO(ME, "module is not running %s, deleting its save file", module);
        delete_module_save_file(module);
        return;
    }

    SAH_TRACEZ_INFO(ME, "restarting module %s", module);
    stop_module(module);
    wait_for_process(module);
}

void handle_impacted_modules(const char* modules) {
    amxc_var_t modules_list;
    const amxc_llist_t* lresult;

    amxc_var_init(&modules_list);
    if(get_modules_list(&modules_list, modules) == 0) {
        lresult = amxc_var_constcast(amxc_llist_t, &modules_list);
        amxc_llist_iterate(it, lresult) {
            amxc_var_t* module = amxc_llist_it_get_data(it, amxc_var_t, lit);
            handle_impacted_module(amxc_var_constcast(cstring_t, module));
        }
    }
    amxc_var_clean(&modules_list);
}
