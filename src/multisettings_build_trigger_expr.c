/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <amxb/amxb.h>
#include "multisettings.h"

struct expression_relational_func {
    const char* op;
    int (* func)(amxd_object_t*, amxc_string_t*);
};

struct relational_operator_mnemo {
    const char* op;
    const char* mnemo;
};

static int build_equal_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.to == '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static int build_equal_not_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.from == '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static int build_not_equal_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.to != '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static int build_not_equal_not_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.from != '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static int build_matches_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.to matches '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static int build_matches_not_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* param, * value;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    param = amxd_path_get_param(&path);
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "notification in 'dm:object-changed' && parameters.%s.from matches '%s'", param, value);
    amxd_path_clean(&path);
    return (0);
}

static struct expression_relational_func relfunc_tab[] = {
    {"Equal", build_equal_expression_from_trigger},
    {"NotEqual", build_not_equal_expression_from_trigger},
    {"Matches", build_matches_expression_from_trigger}
};

static struct expression_relational_func relnotfunc_tab[] = {
    {"Equal", build_equal_not_expression_from_trigger},
    {"NotEqual", build_not_equal_not_expression_from_trigger},
    {"Matches", build_matches_not_expression_from_trigger}
};

static int call_expr_func_from_rela(amxd_object_t* trigger, amxc_string_t* expr) {
    const char* rela_op = GET_CHAR(amxd_object_get_param_value(trigger, "RelationalOperator"), NULL);

    for(unsigned long int i = 0; i < sizeof(relfunc_tab) / sizeof(*relfunc_tab); ++i) {
        if(strcmp(relfunc_tab[i].op, rela_op) == 0) {
            return ((relfunc_tab[i].func)(trigger, expr));
        }
    }
    return (-1);
}

static int call_expr_not_func_from_rela(amxd_object_t* trigger, amxc_string_t* expr) {
    const char* rela_op = GET_CHAR(amxd_object_get_param_value(trigger, "RelationalOperator"), NULL);

    for(unsigned long int i = 0; i < sizeof(relnotfunc_tab) / sizeof(*relnotfunc_tab); ++i) {
        if(strcmp(relnotfunc_tab[i].op, rela_op) == 0) {
            return ((relnotfunc_tab[i].func)(trigger, expr));
        }
    }
    return (-1);
}

static struct relational_operator_mnemo mnemo_tab[] = {
    {"Equal", "=="},
    {"NotEqual", "!="},
    {"Matches", "matches"}
};

static const char* get_operator_mnemo(const char* operator) {
    for(unsigned int i = 0; i < sizeof(mnemo_tab) / sizeof(*mnemo_tab); ++i) {
        if(strcmp(mnemo_tab[i].op, operator) == 0) {
            return (mnemo_tab[i].mnemo);
        }
    }
    return (NULL);
}

int build_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxc_string_init(expr, 0);
    return (call_expr_func_from_rela(trigger, expr));
}

int build_not_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxc_string_init(expr, 0);
    return (call_expr_not_func_from_rela(trigger, expr));
}

int build_expression_get_from_trigger(amxd_object_t* trigger, amxc_string_t* expr) {
    amxd_path_t path;
    const char* value;

    amxc_string_init(expr, 0);
    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    value = GET_CHAR(amxd_object_get_param_value(trigger, "RightMember"), NULL);
    amxc_string_setf(expr, "0.[%s %s '%s']", amxd_path_get_param(&path), get_operator_mnemo(GET_CHAR(amxd_object_get_param_value(trigger, "RelationalOperator"), NULL)), value);
    amxd_path_clean(&path);
    return (0);
}