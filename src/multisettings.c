/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <debug/sahtrace.h>
#include "multisettings.h"

static multisettings_plugin_app_t app;

static void set_multisettings_status(const char* status) {
    amxd_object_t* ms = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, ms);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_apply(&trans, multisettings_plugin_get_dm());
    amxd_trans_clean(&trans);
    SAH_TRACEZ_INFO(ME, "Setting multisettings status to [%s]", status);
}

void start_multisettings(void) {
    amxd_object_t* ms = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    const char* decision_made_by = GET_CHAR(amxd_object_get_param_value(ms, "DecisionMadeBy"), NULL);
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, ms);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    set_multisettings_status("Up");
    if(GET_BOOL(amxd_object_get_param_value(ms, "DetectionAtBoot"), false) || (strcmp(decision_made_by, "NotTaken") == 0)) {
        amxd_trans_set_value(cstring_t, &trans, "DetectionState", "Detecting");
        amxd_trans_apply(&trans, multisettings_plugin_get_dm());
        init_profiles();
    } else {
        SAH_TRACEZ_NOTICE(ME, "Profile was detected previously, applying saved value");
        amxd_trans_set_value(cstring_t, &trans, "DetectionState", "Found");
        amxd_trans_apply(&trans, multisettings_plugin_get_dm());
        const amxc_var_t* current_profile = amxd_object_get_param_value(ms, "CurrentProfile");
        write_profile(amxc_var_constcast(cstring_t, current_profile));
    }
    amxd_trans_clean(&trans);
}

void _multisettings_start(UNUSED const char* const sig_name,
                          UNUSED const amxc_var_t* const data,
                          UNUSED void* const priv) {
    amxd_object_t* multisettings = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    if(amxd_object_get_bool(multisettings, "Enable", NULL)) {
        start_multisettings();
    } else {
        set_multisettings_status("Down");
    }
}

void stop_multisettings(void) {
    deinit_profiles();
    set_multisettings_status("Down");
    unlink(multisettings_plugin_get_selected_profile_file());
}

void restart_multisettings(void) {
    stop_multisettings();
    start_multisettings();
}

void multisettings_plugin_init(amxd_dm_t* dm, amxo_parser_t* parser) {
    amxc_array_t* uris = amxb_list_uris();
    app.dm = dm;
    app.parser = parser;
    app.bus = amxb_find_uri((const char*) amxc_array_get_data_at(uris, 0));
    amxc_string_init(&app.object_name, 0);
    amxc_string_init(&app.selected_profile, 0);
    amxp_sigmngr_init(&app.sigmngr);
    amxc_array_delete(&uris, NULL);
    SAH_TRACEZ_INFO(ME, "Multisettings init");
}

amxd_dm_t* multisettings_plugin_get_dm(void) {
    return (app.dm);
}

amxp_signal_mngr_t* multisettings_plugin_get_sigmngr(void) {
    return (&app.sigmngr);
}

amxb_bus_ctx_t* multisettings_plugin_get_bus(void) {
    return (app.bus);
}

amxo_parser_t* multisettings_plugin_get_parser(void) {
    return (app.parser);
}

amxc_var_t* multisettings_plugin_get_config(void) {
    return &(app.parser->config);
}

const char* multisettings_plugin_get_selected_profile_file(void) {
    if(amxc_string_is_empty(&app.selected_profile)) {
        amxc_var_t* setting = amxo_parser_get_config(multisettings_plugin_get_parser(), "selected_profile_file");
        amxc_string_set(&app.selected_profile, amxc_var_constcast(cstring_t, setting));
    }
    return amxc_string_get(&app.selected_profile, 0);
}

const char* multisettings_plugin_get_object_name(void) {
    if(amxc_string_is_empty(&app.object_name)) {
        amxc_string_setf(&app.object_name, "%s", MULTISETTINGS_ROOT_NAME);
    }
    return amxc_string_get(&app.object_name, 0);
}

int _multisettings_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser) {
    int retval = 0;

    switch(reason) {
    case 0: // START
        multisettings_plugin_init(dm, parser);
        break;
    case 1: // STOP
        SAH_TRACEZ_INFO(ME, "Multisettings clean up");
        app.dm = NULL;
        app.parser = NULL;
        app.bus = NULL;
        amxc_string_clean(&app.object_name);
        amxc_string_clean(&app.selected_profile);
        amxp_sigmngr_clean(&app.sigmngr);
        break;
    default:
        retval = -1;
        break;
    }
    return retval;
}
