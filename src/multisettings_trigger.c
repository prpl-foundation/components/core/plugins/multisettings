/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include "multisettings.h"

static void set_trigger_value(amxd_object_t* trigger, bool value) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, trigger);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(bool, &trans, "Triggered", value);
    amxd_trans_apply(&trans, multisettings_plugin_get_dm());
    amxd_trans_clean(&trans);
    if(value == true) {
        check_if_profile_match(amxd_object_get_parent(amxd_object_get_parent(trigger)));
    }
}

void amx_expression_triggered(UNUSED const char* const sig_name,
                              UNUSED const amxc_var_t* const data,
                              void* const priv) {
    set_trigger_value((amxd_object_t*) priv, true);
}

void amx_not_expression_triggered(UNUSED const char* const sig_name,
                                  UNUSED const amxc_var_t* const data,
                                  void* const priv) {
    set_trigger_value((amxd_object_t*) priv, false);
}

void object_found_init_trigger(UNUSED const char* const sig_name,
                               UNUSED const amxc_var_t* const data,
                               void* const priv) {
    amxd_object_t* trigger = (amxd_object_t*) priv;
    amxc_var_t ret, * found;
    amxd_path_t path;
    amxc_string_t expr;

    SAH_TRACEZ_INFO(ME, "Trigger object found: %s", GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    amxc_var_init(&ret);
    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    // Subscribe to trigger
    build_expression_from_trigger(trigger, &expr);
    amxb_subscribe(multisettings_plugin_get_bus(), amxd_path_get(&path, AMXD_OBJECT_TERMINATE), amxc_string_get(&expr, 0), amx_expression_triggered, trigger);
    amxc_string_clean(&expr);
    // Subscribe to the inverse trigger
    build_not_expression_from_trigger(trigger, &expr);
    amxb_subscribe(multisettings_plugin_get_bus(), amxd_path_get(&path, AMXD_OBJECT_TERMINATE), amxc_string_get(&expr, 0), amx_not_expression_triggered, trigger);
    amxc_string_clean(&expr);
    // Get actual value to match trigger expression
    amxb_get(multisettings_plugin_get_bus(), amxd_path_get(&path, AMXD_OBJECT_TERMINATE), 1, &ret, 5);
    build_expression_get_from_trigger(trigger, &expr);
    found = amxp_expr_find_var(&ret, amxc_string_get(&expr, 0));
    set_trigger_value(trigger, found != NULL);
    amxc_string_clean(&expr);
    amxc_var_clean(&ret);
    amxd_path_clean(&path);
}

void init_trigger(amxd_object_t* trigger) {
    amxd_path_t path;
    amxc_string_t signal;

    amxc_string_init(&signal, 0);
    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    amxc_string_setf(&signal, "wait:%s", amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
    amxb_wait_for_object(amxd_path_get(&path, AMXD_OBJECT_TERMINATE));
    amxp_slot_connect(NULL, amxc_string_get(&signal, 0), NULL, object_found_init_trigger, trigger);
    amxc_string_clean(&signal);
    amxd_path_clean(&path);
}

void deinit_trigger(amxd_object_t* trigger) {
    amxd_path_t path;

    amxd_path_init(&path, GET_CHAR(amxd_object_get_param_value(trigger, "LeftMember"), NULL));
    set_trigger_value(trigger, false);
    amxp_slot_disconnect_with_priv(NULL, object_found_init_trigger, trigger);
    amxb_unsubscribe(multisettings_plugin_get_bus(), amxd_path_get(&path, AMXD_OBJECT_TERMINATE), amx_expression_triggered, trigger);
    amxb_unsubscribe(multisettings_plugin_get_bus(), amxd_path_get(&path, AMXD_OBJECT_TERMINATE), amx_not_expression_triggered, trigger);
    amxd_path_clean(&path);
}
