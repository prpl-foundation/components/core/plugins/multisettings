/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_path.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>
#include "multisettings.h"

static amxp_signal_t* profile_found_sig = NULL;

static void set_multisettings_profile_status(amxd_object_t* profile, const char* status) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, profile);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_trans_set_value(cstring_t, &trans, "Status", status);
    amxd_trans_apply(&trans, multisettings_plugin_get_dm());
    amxd_trans_clean(&trans);
    SAH_TRACEZ_INFO(ME, "Profile [%s] status set to [%s]",
                    GET_CHAR(amxd_object_get_param_value(profile, "Name"), NULL), status);
}

void profile_found(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    amxd_object_t* multisettings = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    amxd_object_t* profile = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", GET_CHAR(data, NULL));
    const char* name = GET_CHAR(amxd_object_get_param_value(profile, "Name"), NULL);

    SAH_TRACEZ_NOTICE(ME, "Profile found (%s)", name);
    amxd_object_set_cstring_t(multisettings, "CurrentProfile", name);
    amxd_object_set_cstring_t(multisettings, "DetectionState", "Found");
    amxd_object_set_cstring_t(multisettings, "DecisionMadeBy", "Detection");
    deinit_profiles();
    write_profile(name);
    handle_impacted_modules(GET_CHAR(amxd_object_get_param_value(profile, "ImpactedModules"), NULL));
}

void check_if_profile_match(amxd_object_t* profile) {
    bool profile_match = true;
    amxd_status_t status;
    amxd_object_t* triggers = amxd_object_get_child(profile, "Trigger"), * t;
    amxc_var_t profile_name;
    char* path;

    amxd_object_iterate(instance, it, triggers) {
        t = amxc_container_of(it, amxd_object_t, it);
        if(amxd_object_get_bool(t, "Triggered", &status) == false) {
            profile_match = false;
        }
    }
    if(profile_match) {
        amxc_var_init(&profile_name);
        path = amxd_object_get_path(profile, AMXD_OBJECT_INDEXED);
        amxc_var_set(cstring_t, &profile_name, path);
        amxp_sigmngr_trigger_signal(multisettings_plugin_get_sigmngr(), "profile_found", (const amxc_var_t*) &profile_name);
        free(path);
        amxc_var_clean(&profile_name);
    }
}

void init_profile(amxd_object_t* profile) {
    amxd_status_t status;
    amxd_object_t* triggers;

    if(amxd_object_get_bool(profile, "Enable", &status) && (status == amxd_status_ok)) {
        triggers = amxd_object_get_child(profile, "Trigger");
        set_multisettings_profile_status(profile, "Pending");
        amxd_object_iterate(instance, it_trig, triggers) {
            init_trigger(amxc_container_of(it_trig, amxd_object_t, it));
        }
    } else {
        set_multisettings_profile_status(profile, "Inactive");
    }
}

void init_profiles(void) {
    amxd_object_t* profiles = amxd_dm_findf(multisettings_plugin_get_dm(), "%s.Profile", multisettings_plugin_get_object_name());

    SAH_TRACEZ_INFO(ME, "Init profiles");
    amxp_signal_new(multisettings_plugin_get_sigmngr(), &profile_found_sig, "profile_found");
    amxp_slot_connect(multisettings_plugin_get_sigmngr(), "profile_found", NULL, profile_found, NULL);
    amxd_object_iterate(instance, it, profiles) {
        init_profile(amxc_container_of(it, amxd_object_t, it));
    }
}

void deinit_profile(amxd_object_t* profile) {
    amxd_object_t* triggers = amxd_object_get_child(profile, "Trigger");

    amxd_object_iterate(instance, it, triggers) {
        deinit_trigger(amxc_container_of(it, amxd_object_t, it));
    }
    set_multisettings_profile_status(profile, "Inactive");
}

void deinit_profiles(void) {
    amxd_object_t* profiles = amxd_dm_findf(multisettings_plugin_get_dm(), "%s.Profile", multisettings_plugin_get_object_name());

    SAH_TRACEZ_INFO(ME, "Deinit profiles");
    amxd_object_iterate(instance, it, profiles) {
        deinit_profile(amxc_container_of(it, amxd_object_t, it));
    }
    amxp_signal_delete(&profile_found_sig);
    // Reset connected slot
    amxp_sigmngr_clean(multisettings_plugin_get_sigmngr());
    amxp_sigmngr_init(multisettings_plugin_get_sigmngr());
}
