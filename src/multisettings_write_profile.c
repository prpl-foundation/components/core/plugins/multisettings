/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_types.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include "multisettings.h"

static bool profile_exist(const char* profile) {
    bool retval = false;
    amxc_llist_t paths;

    amxc_llist_init(&paths);
    amxd_dm_resolve_pathf(multisettings_plugin_get_dm(), &paths, "%s.Profile.[Name == '%s'].", multisettings_plugin_get_object_name(), profile);

    retval = !amxc_llist_is_empty(&paths);
    amxc_llist_clean(&paths, amxc_string_list_it_free);

    return retval;
}

static amxd_object_t* find_profile_dm(const char* profile_name) {
    amxd_object_t* profiles = amxd_dm_findf(multisettings_plugin_get_dm(), "%s.Profile", multisettings_plugin_get_object_name());
    amxd_object_t* temp;

    amxd_object_iterate(instance, it, profiles) {
        temp = amxc_container_of(it, amxd_object_t, it);
        if(strcmp(profile_name, GET_CHAR(amxd_object_get_param_value(temp, "Name"), NULL)) == 0) {
            return temp;
        }
    }
    return NULL;
}

void write_profile_file(const char* profile) {
    int fd;

    fd = open(multisettings_plugin_get_selected_profile_file(), O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if(fd != -1) {
        dprintf(fd, "%%config {\n\t%%global profile = \"%s\";\n}\n", profile);
        close(fd);
    } else {
        SAH_TRACEZ_ERROR(ME, "Unable to write profile file. Error: %d (%s)", errno, strerror(errno));
    }
}

void write_profile(const char* profile) {
    amxd_object_t* multisettings = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    if(!amxd_object_get_bool(multisettings, "Enable", NULL)) {
        return;
    }
    amxd_object_t* profiles = amxd_dm_findf(multisettings_plugin_get_dm(), "%s.Profile", multisettings_plugin_get_object_name()), * p;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);
    amxd_object_iterate(instance, it, profiles) {
        p = amxc_container_of(it, amxd_object_t, it);
        amxd_trans_select_object(&trans, p);
        if(strcmp(profile, GET_CHAR(amxd_object_get_param_value(p, "Name"), NULL)) == 0) {
            amxd_trans_set_value(cstring_t, &trans, "Status", "Active");
        } else {
            amxd_trans_set_value(cstring_t, &trans, "Status", "Inactive");
        }
    }
    amxd_trans_apply(&trans, multisettings_plugin_get_dm());
    amxd_trans_clean(&trans);
    write_profile_file(profile);
}

void _writeProfile(UNUSED const char* const sig_name,
                   const amxc_var_t* const data,
                   UNUSED void* const priv) {
    const char* profile = GETP_CHAR(data, "parameters.CurrentProfile.to");
    const char* old_profile = GETP_CHAR(data, "parameters.CurrentProfile.from");
    amxd_object_t* multisettings = amxd_dm_findf(multisettings_plugin_get_dm(), "%s", multisettings_plugin_get_object_name());
    amxd_trans_t trans;
    amxd_trans_init(&trans);
    amxd_trans_select_object(&trans, multisettings);
    amxd_trans_set_attr(&trans, amxd_tattr_change_ro, true);

    if(*profile) {
        if(profile_exist(profile)) {
            amxd_object_t* profile_dm = find_profile_dm(profile);
            amxd_trans_set_value(cstring_t, &trans, "DetectionState", "Found");
            if(GETP_CHAR(data, "parameters.DecisionMadeBy.to") == NULL) {
                amxd_trans_set_value(cstring_t, &trans, "DecisionMadeBy", "System");
            }
            amxd_trans_apply(&trans, multisettings_plugin_get_dm());
            if(amxd_object_get_bool(multisettings, "Enable", NULL)) {
                deinit_profiles();
                write_profile(profile);
                handle_impacted_modules(GET_CHAR(amxd_object_get_param_value(profile_dm, "ImpactedModules"), NULL));
                SAH_TRACEZ_NOTICE(ME, "profile changed [%s] -> [%s]", old_profile, profile);
            }
        } else {
            SAH_TRACEZ_ERROR(ME, "Invalid profile [%s], change not applied", profile);
            //non existent profile, revert changes
            amxd_object_set_cstring_t(multisettings, "CurrentProfile", old_profile);
        }
    } else {
        amxd_trans_set_value(cstring_t, &trans, "DetectionState", "Inactive");
        amxd_trans_set_value(cstring_t, &trans, "DecisionMadeBy", "NotTaken");
        unlink(multisettings_plugin_get_selected_profile_file());

        //restart old profile modules so they can fallback into their default settings
        if(profile_exist(old_profile)) {
            amxd_object_t* profile_dm = find_profile_dm(old_profile);
            handle_impacted_modules(GET_CHAR(amxd_object_get_param_value(profile_dm, "ImpactedModules"), NULL));
        }
        amxd_trans_apply(&trans, multisettings_plugin_get_dm());
        restart_multisettings();
    }
    amxd_trans_clean(&trans);
}
