# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.11.2 - 2024-07-23(16:32:49 +0000)

### Fixes

- Better shutdown script

## Release v0.11.1 - 2024-04-10(09:59:56 +0000)

### Changes

- Make amxb timeouts configurable

## Release v0.11.0 - 2024-03-23(13:08:42 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v0.10.0 - 2024-02-27(17:32:39 +0000)

### Fixes

- Change start order to start before ip-manager

## Release v0.9.1 - 2023-11-27(10:45:56 +0000)

### Other

- Source environment for autoprofile

## Release v0.9.0 - 2023-10-17(11:01:33 +0000)

### New

- [PRPLOS][FUT][Multisetting]profile needs to be upgrade persistent

## Release v0.8.1 - 2023-10-11(12:37:02 +0000)

### Other

- [multisettings] Add support for defaults.d directory

## Release v0.8.0 - 2023-08-31(09:32:41 +0000)

### New

- Move datamodel prefixes to the device proxy

## Release v0.7.2 - 2023-02-15(14:45:30 +0000)

### Other

- Add verbosity/debug logs to Multisetting component

## Release v0.7.1 - 2023-01-23(09:36:32 +0000)

### Fixes

- Cannot Disable multisettings service

## Release v0.7.0 - 2022-12-15(14:11:27 +0000)

### Fixes

- [multisettings] Using triggers is not effective

## Release v0.6.1 - 2022-11-15(10:02:13 +0000)

### Fixes

- Restart via amxp_subproc_t not working [PROVISORY]

## Release v0.6.0 - 2022-10-28(09:14:56 +0000)

### Fixes

- ImpactedModules not restarted when profile Updated
- Profile not applied after reboot
- Issue: PROVISORY HOP-2121 Reset ImpactedModules save file when profile is changed

## Release v0.5.4 - 2022-06-23(17:22:30 +0000)

### Other

- [amxrt] All amx plugins should start with the -D option

## Release v0.5.3 - 2022-06-09(06:17:26 +0000)

### Other

- Dir path in odl file has one dash too much

## Release v0.5.2 - 2022-05-23(07:42:34 +0000)

### Fixes

- [Gitlab CI][Unit tests][valgrind] Pipeline doesn't stop when memory leaks are detected

## Release v0.5.1 - 2022-03-24(10:59:23 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v0.5.0 - 2022-02-10(13:23:02 +0000)

### New

- - Add matches trigger and some unit test

## Release v0.4.0 - 2022-02-04(18:51:40 +0000)

### New

- Add change notification, refactor set_xxx to set_value

## Release v0.3.0 - 2022-01-24(12:37:10 +0000)

### New

- Restart impacted modules when profile is found
- Add a parameter to configure detection at each boot

## Release v0.2.0 - 2022-01-21(09:11:37 +0000)

### New

- Reload Trigger

### Fixes

- Fix memory leaks

### Other

- Add unit tests

## Release v0.1.0 - 2021-12-18(18:47:36 +0000)

### New

- Create the multisettings manager with its data-model
- Handle selected profile
- Handle profile selection using different kinds of triggers
- Handle amx expression trigger

