/****************************************************************************
**
** Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/


%define {

	/** 
	 * Object describing profiles and how to select them.
	 * Once a profile has been selected detection will stop. (See CurrentProfile to reset profile selection)
	 * @version V1.0
	 */
	%persistent object 'MultiSettings' {
		/**
		* Enable Profile detection
		* @version V1.0
		*/
		%persistent bool Enable = true;

		/**
		* The current operational state of the interface (see [Section 4.2.2/TR-181i2]). Enumeration of:
		* Up
		* Down
		* Unknown
		* Dormant
		* NotPresent
		* LowerLayerDown
		* Error (OPTIONAL)
		* @version V1.0
		*/
		%read-only string Status = "Down" {
			on action validate call check_enum ["Up", "Down", "Unknown", "Dormant", "NotPresent", "LowerLayerDown", "Error"];
		}

		/**
		* Contains the current profile name. This parameter will be used to fill the SELECTED_PROFILE_FILE.
		* Can be set manually.
		* If set to empty string, the profile selection will be reset and redetection will occurs.
		* @version V1.0
		*/
		%persistent string CurrentProfile {
			userflags %upc;
		}

		/**
		* The current detection state. Enumeration of:
		* Found
		* Detecting
		* Inactive
		* @version V1.0
		*/
		%read-only string DetectionState = "Inactive" {
			on action validate call check_enum ["Found", "Detecting", "Inactive"];
		}

		/**
		* Contains how the decision has been made.
		* If the value is NotTaken detection will occur (see CurrentProfile to reset detection).
		* Enumeration of:
		* System
		* Detection
		* NotTaken
		* @version V1.0
		*/
		%read-only %persistent string DecisionMadeBy = "NotTaken" {
			on action validate call check_enum ["System", "Detection", "NotTaken"];
		}

		/**
		* Enable profile detection after each boot
		* @version V1.0
		*/
		%persistent bool DetectionAtBoot = false;

		/**
		* This object describe the profile.
		* @version V1.0
		*/
		%persistent object Profile[] {
			/**
			* Number of Entries for Profile
			* @version V1.0
			*/
			counted with ProfileNumberOfEntries;

			/**
			* Alias of the profile.
			* @version V1.0
			*/
			%unique %key %persistent string Alias;

			/**
			* Enable the profile during detection phase
			* @version V1.0
			*/
			%persistent bool Enable = true {
				userflags %upc;
			}
			
			/**
			* The current status of the profile. Enumeration of:
			* Active
			* Pending
			* Inactive
			* Error
			* @version V1.0
			*/
			%read-only string Status = "Pending" {
				on action validate call check_enum ["Active", "Pending", "Inactive", "Error"];
			}

			/**
			* The name of the profile. It is not a unique ID, multiple profile can have the same name and describe different selection method.
			* @version V1.0
			*/
			%persistent string Name {
				userflags %upc;
			}

			/**
			* The list of impacted modules, in the format of csv
			* @version V1.0
			*/
			%persistent csv_string ImpactedModules {
				userflags %upc;
			}

			/**
			* This object describe an expression.
			* During detection phase, if all Triggers in the list are true then the profile will be selected.
			* @version V1.0
			*/
			%persistent object Trigger[] {
				/**
				* Number of Entries for Trigger
				* @version V1.0
				*/
				counted with TriggerNumberOfEntries;

				/**
				* Alias of the trigger.
				* @version V1.0
				*/
				%unique %key %persistent string Alias;

				/**
				* During detection phase, reflect if the expression is true or false
				* @version V1.0
				*/
				%read-only bool Triggered = false;

				/**
				* Contains the path to a parameter in the data-model that will be assessed with the RightMember.
				* @version V1.0
				*/
				%persistent string LeftMember = "" {
					userflags %upc;
				}

				/**
				* Contains the relational operator between the left and right member. Enumeration of:
				* Equal
				* NotEqual
				* Matches
				* @version V1.0
				*/
				%persistent string RelationalOperator = "Equal" {
					on action validate call check_enum ["Equal", "NotEqual", "Matches"];
					userflags %upc;
				}				

				/**
				* Contains the value the LeftMember will be assessed with.
				* @version V1.0
				*/
				%persistent string RightMember = "" {
					userflags %upc;
				}
			}
		}
	}
}
