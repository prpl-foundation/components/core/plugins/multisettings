/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__multisettings_H__)
#define __multisettings_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxp/amxp_signal.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>
#include <amxb/amxb.h>
#include <debug/sahtrace.h>

#define PRIVATE __attribute__ ((visibility("hidden")))
#define UNUSED __attribute__((unused))

typedef struct _multisettings_plugin_app {
    amxd_dm_t* dm;
    amxo_parser_t* parser;
    amxb_bus_ctx_t* bus;
    amxp_signal_mngr_t sigmngr;
    amxc_string_t object_name;
    amxc_string_t selected_profile;
} multisettings_plugin_app_t;

#define MULTISETTINGS_ROOT_NAME "MultiSettings"

#define ME "multisettings"

amxd_dm_t* multisettings_plugin_get_dm(void);
amxb_bus_ctx_t* multisettings_plugin_get_bus(void);
amxc_var_t* multisettings_plugin_get_config(void);
amxp_signal_mngr_t* multisettings_plugin_get_sigmngr(void);
amxo_parser_t* multisettings_plugin_get_parser(void);
const char* multisettings_plugin_get_object_name(void);
const char* multisettings_plugin_get_selected_profile_file(void);
int _multisettings_main(int reason, amxd_dm_t* dm, amxo_parser_t* parser);
void start_multisettings(void);
void stop_multisettings(void);
void restart_multisettings(void);
void init_profiles(void);
void init_profile(amxd_object_t* profile);
void init_trigger(amxd_object_t* trigger);
void deinit_profiles(void);
void deinit_profile(amxd_object_t* profile);
void deinit_trigger(amxd_object_t* trigger);
int build_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr);
int build_not_expression_from_trigger(amxd_object_t* trigger, amxc_string_t* expr);
int build_expression_get_from_trigger(amxd_object_t* trigger, amxc_string_t* expr);
void check_if_profile_match(amxd_object_t* profile);
void write_profile(const char* profile);
void write_profile_file(const char* profile);
void handle_impacted_modules(const char* modules);
int module_is_running(const char* module);
int get_modules_list(amxc_var_t* list, const char* modules);
void multisettings_plugin_init(amxd_dm_t* dm, amxo_parser_t* parser);
void amx_expression_triggered(const char* const sig_name,
                              const amxc_var_t* const data,
                              void* const priv);
void amx_not_expression_triggered(const char* const sig_name,
                                  const amxc_var_t* const data,
                                  void* const priv);
void object_found_init_trigger(const char* const sig_name,
                               const amxc_var_t* const data,
                               void* const priv);
void profile_found(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);
void _writeProfile(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);
void _writeEnable(const char* const sig_name,
                  const amxc_var_t* const data,
                  void* const priv);
void _writeTrigger(const char* const sig_name,
                   const amxc_var_t* const data,
                   void* const priv);
void _multisettings_start(const char* const sig_name,
                          const amxc_var_t* const data,
                          void* const priv);

#ifdef __cplusplus
}
#endif

#endif // __multisettings_H__
